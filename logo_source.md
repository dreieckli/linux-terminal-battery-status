The logo was created using the following artwork as a source:

* The terminal was fetched from https://commons.wikimedia.org/wiki/File:Antu_x-terminal-emulator.svg on 2023-07-17. The license of this file is "[Creative Commons Attribution-Share Alike 3.0 Unported](https://creativecommons.org/licenses/by-sa/3.0/deed.en)".
* The battery icon was fetched from the [Arch Linux package `openclipart-svg`](https://aur.archlinux.org/packages/openclipart-svg), version 0.18-2. The license of this icon is Public Domain.

The resulting license of the Logo is "[Creative Commons Attribution-Share Alike 3.0 Unported](https://creativecommons.org/licenses/by-sa/3.0/deed.en)", the license text can be read in the file [`COPYING.logo.txt`](COPYING.logo.txt).

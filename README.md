# linux-terminal-battery-status

This is a simple script utility that shows the basic stats of battery to the user.


## Dependencies

### Mandatory dependencies:

* A Linux system with battery information in `/sys/class/power_supply/BAT*` and power power supply information in `/sys/class/power_supply/A*`,
* `bash` for executing the script,
* `basename` for some string truncation (in Arch Linux, it is part of the package "coreutils"),
* `awk` for calculations and formatting (in Arch Linux, it is part of the package "gawk").

### Optional dependencies:

* `acpi`, `grep` and `sed` to show remaining charging/ discharging time.
* `kdeconnect-cli` and `qdbus` to show data from mobile devices connected to the system via KDE Connect. (In Arch Linux, `kdeconnect-cli` is part of the package "kdeconnect", and `qdbus` is part of the package `qt5-tools`.)

## Installation

Copy `linux-terminal-battery-status.sh` to a place where you can run it from (e.g. `/usr/local/bin/linux-terminal-battery-status` or `${HOME}/bin/linux-terminal-battery-status`) and make it executable (`chmod a+x [...]/linux-terminal-battery-status`).

### Arch Linux and derived distributions

Install from the AUR: [`linux-terminal-battery-status-git`](https://aur.archlinux.org/packages/linux-terminal-battery-status-git).

## Example

Example output of `linux-terminal-battery-status`:

```
=== BAT0: ===
Status:  97% Charging (present), 00:11:47 until charged
Charge:  20.49 Wh / 20.91 Wh = 98.0%
Health:  20.91 Wh / 23.56 Wh = 88.8%
Power:  +2.17 W
Voltage: 7.60 V
Alarm:   0.00 Wh

=== ADP1 (Mains): ===
Status:  online

=== ucsi-source-psy-USBC000:001 (USB [C] PD PD_PPS): ===
Status:  offline

=== ucsi-source-psy-USBC000:002 (USB [C] PD PD_PPS): ===
Status:  offline
```

The main bash script was taken from https://github.com/niksingh710/basic-battery-stat on 2023-07-17 (https://github.com/niksingh710/basic-battery-stat/blob/31361177c152ccddc9e20fe4796662d9b51ebd4c/basic-battery-stat)

The original license allows to do that; the original license text as of 2023-07-17 (https://github.com/niksingh710/basic-battery-stat/blob/31361177c152ccddc9e20fe4796662d9b51ebd4c/LICENSE) is/was:

```
            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
                    Version 2, December 2004

 Copyright (C) 2004 Sam Hocevar <sam@hocevar.net>

 Everyone is permitted to copy and distribute verbatim or modified
 copies of this license document, and changing it is allowed as long
 as the name is changed.

            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
   TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION

  0. You just DO WHAT THE FUCK YOU WANT TO.
```
